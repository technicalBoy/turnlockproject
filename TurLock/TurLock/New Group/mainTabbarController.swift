//
//  mainTabbarController.swift
//  AboutLove
//
//  Created by wanglongfie on 2019/8/21.
//  Copyright © 2019年 wanglongfei. All rights reserved.
//

import UIKit
class mainTabbarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addChildViewControllers()
    }
    
    
    func addChildViewControllers() {
        
        let equipstrory = UIStoryboard.init(name: "equipViewController", bundle: nil)
        let equipVC = equipstrory.instantiateViewController(withIdentifier: "equipViewController") as! equipViewController
        configChildViewController(childViewController: equipVC, title: "设备", imageName: "tabbar_equip_nore", selectedImageName: "tabbar_equip_select")
        
          let nav = UINavigationController.init(rootViewController: equipVC)
        
        let  infoVC = infoViewController()
        configChildViewController(childViewController: infoVC, title: "资讯", imageName: "tabbar_info_nore", selectedImageName: "tabbar_info_select")
        
        
        let mineVC = mineViewController()
      
        configChildViewController(childViewController: mineVC, title: "我的", imageName: "tabbar_mine_nore", selectedImageName: "tabbar_mine_select")

        self.viewControllers = [nav, infoVC, mineVC]
        self.selectedIndex = 0
        
    }
    
    override func viewWillLayoutSubviews() {
//        var tabbarFrame = self.tabBar.frame
//        tabbarFrame.size.height = (W_IphoneX ? 83 : 49) + 20
//        tabbarFrame.origin.y = W_IphoneX ? KScreenHeight - 83 : KScreenHeight - 49
//        self.tabBar.frame = tabbarFrame
    }
    
    private func configChildViewController(childViewController: UIViewController, title: String?, imageName: String, selectedImageName: String){
        
        childViewController.title = title
        childViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 0)
        childViewController.tabBarItem.image = UIImage.init(named: imageName)?.withRenderingMode(.alwaysOriginal)
        
        childViewController.tabBarItem.selectedImage = UIImage.init(named: selectedImageName)?.withRenderingMode(.alwaysOriginal)

    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
