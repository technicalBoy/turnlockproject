//
//  sendData.swift
//  Swift-BLE
//
//  Created by wlf on 2019/9/2.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
//秘钥

//let  secretkey: [UInt8] = [3A, 60, 43, 2A, 5C, 01, 21, 1F, 29, 1E, 0F, 4E, 0C, 13, 28, 25]
//token
let  tokenBytes :[UInt8]    = [0x01, 0x01, 0x00, 0x00]

//开锁
let  unlockBytes :[UInt8]    = [0x02, 0x01, 0x0a, 0x00]

//关锁
let  lockBytes :[UInt8]    = [0x03, 0x01, 0x0a, 0x00]

//锁状态
let  statusBytes :[UInt8]    = [0x04, 0x01, 0x04, 0x00]

//查询电量
let  batteryBytes :[UInt8]    = [0x05, 0x01, 0x04, 0x00]

//1.8    设置锁密码
let  setPwdBytes :[UInt8]    = [0x05, 0x01, 0x04, 0x00]

//1.9    设置锁秘钥
