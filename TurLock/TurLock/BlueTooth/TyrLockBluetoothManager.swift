//
//  TyrLockBluetoothManager.swift
//  TurLock
//
//  Created by wlf on 2019/9/15.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import CoreBluetooth
import SVProgressHUD
class TurLockBluetoothManager: NSObject {
    
    static var share = TurLockBluetoothManager()
    override init() {
        
    }
    
    lazy var centralManager: CBCentralManager = {
        
        let c = CBCentralManager.init()
        
        return c
    }()
    
    var connectedPeripheral: CBPeripheral?
    var discoveredPeripheralsArr: [CBPeripheral] = []
    var signalRSSIArr: [NSNumber] = []
    
    //蓝牙加密认证  服务CBService的UUID
    let  confirmServiceUUID = "0xFFF0"
    
    //设备特征
    var readCharacteristic: CBCharacteristic!
    //蓝牙加密认证
    let readCharacteristicUUID = "0xFFF3"
    
    //char[4]
    var notifiCharacteristic: CBCharacteristic!
    let notifiCharacteristicUUID = "0xFFF2"
    
    //char[6]
    var writeCharacteristic: CBCharacteristic!
    let writeCharacteristicUUID = "0xFFF1"
    
    //通知的描述
    var  notifiCharacteristicUUID_DES2Descriptor:CBDescriptor!
    let notifiCharacteristicUUID_DES2 = "00002902-0000-1000-8000-XXXXXXXXXXX"
}

extension TurLockBluetoothManager{
    
    //启动蓝牙
    func bluetoohStar()  {
        
        self.centralManager.delegate = self
        self.centralManager.scanForPeripherals(withServices: nil, options: nil)
    }
    
   // 链接外设
    func connect(peripheral: CBPeripheral) {
        self.connectedPeripheral = peripheral
        centralManager.connect(peripheral, options: nil)
    }
    
    func cancelScan() {
        centralManager.stopScan()
    }
    
    func writeToPeripheral(_ data: Data) {
        self.connectedPeripheral?.writeValue(data , for: writeCharacteristic!, type: CBCharacteristicWriteType.withResponse)
    }
  
    func requestConnectPeripheral(_ model:CBPeripheral) {
        if (model.state != CBPeripheralState.connected) {
            centralManager.connect(model , options: nil)
        }
    }
   
}

extension TurLockBluetoothManager: CBCentralManagerDelegate{
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
        case .unknown:
            print("CBCentralManager state:", "unknown")
        case .resetting:
            print("CBCentralManager state:", "resetting")
        case .unsupported:
            print("CBCentralManager state:", "unsupported")
        case .unauthorized:
            print("CBCentralManager state:", "unauthorized")
        case .poweredOn:
            print("CBCentralManager state:", "poweredOn")
            ///扫描设备
//            central.scanForPeripherals(withServices: nil, options: nil)
        case .poweredOff:
            print("CBCentralManager state:", "poweredOff")
        default:
            print("未知错误")
        }
    }
    
    //发现设备
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        if peripheral.name == DevictName {

            self.centralManager.connect(peripheral, options: nil)
            self.centralManager = central
        }
        
    }
    
    //连接设备成功
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        self.connectedPeripheral = peripheral
        peripheral.delegate = self
        self.centralManager.stopScan()
        //开始寻找Services,
        peripheral.discoverServices(nil)
        
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        
        SVProgressHUD.showError(withStatus: "连接失败")
    }
    
    //断开连接
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        SVProgressHUD.showError(withStatus: "断开连接")
        
    }
    
}

extension TurLockBluetoothManager: CBPeripheralDelegate{
    
    //寻找服务
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        if error != nil {
            
            
        }
        
        //服务的id: 0xFFF0
        if let services = peripheral.services{
            
            for service in services {
                
                if service.uuid.uuidString == "0xFFF0"{
                    
                    peripheral.discoverCharacteristics(nil, for: service)
                }
                
            }
            
        }
        
    }
    
    
    //从感兴趣的服务中, 确定特征
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        if error != nil {
            SVProgressHUD.showError(withStatus: error?.localizedDescription)
        }
        
        for characteristic in service.characteristics! {
            
            //所有特征
            let propertie = characteristic.properties
            
            if propertie == CBCharacteristicProperties.notify{
                
                peripheral.setNotifyValue(true, for: characteristic)
            }
            if propertie == CBCharacteristicProperties.write{
                
                
            }
            
            if propertie == CBCharacteristicProperties.read{
                
                peripheral.readValue(for: characteristic)
            }
            
            //char[3]
            if characteristic.uuid.uuidString == writeCharacteristicUUID {
                
                self.writeCharacteristic = characteristic
                //写入
                let byte:[UInt8] = [0xAA]
                let data = Data(bytes: tokenBytes, count: 20)
                
                for byte in 0..<data.count {
                    print("\(byte)")
                }
                self.connectedPeripheral!.writeValue(data, for: self.writeCharacteristic, type: CBCharacteristicWriteType.withResponse)
                
                
            }
            //char[4]
            if characteristic.uuid.uuidString == notifiCharacteristicUUID {
                
                self.notifiCharacteristic = characteristic
                
                if let descriptors = characteristic.descriptors {
                    
                    for descriptor in descriptors {
                        if descriptor.uuid.uuidString == notifiCharacteristicUUID_DES2 {
                            self.notifiCharacteristicUUID_DES2Descriptor = descriptor
                            
                        }
                    }
                }
                //设置char[4]的通知 来确定是否认证完成
                self.connectedPeripheral!.setNotifyValue(true, for: self.notifiCharacteristic)
                
            }
            //char[6]
            if characteristic.uuid.uuidString ==  readCharacteristicUUID{
                
        
               print(self.readCharacteristic.value)
                
                
            }
            
        }
        
    }
    
    
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if error != nil {
            
            print("didWriteValueForCharacteristic")
            
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if error != nil {
            print(error?.localizedDescription)
        }
        
        
        
    }
    
}
