//
//  ClientViewController.swift
//  TurLock
//
//  Created by wlf on 2019/9/1.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit
import FZBluetooth
import SVProgressHUD
class ClientViewController:  UIViewController{
    var centralManager: CBCentralManager?
    override func viewDidLoad() {
        
        //扫描外设
        FzhBluetooth.shareInstance()?.scanForPeripherals(withPrefixName: DevictName, discoverPeripheral: { (centralManager, peripheral, hashable, RSSINum) in
            
            self.centralManager = centralManager
            
            //链接外设
            FzhBluetooth.shareInstance()?.connect(peripheral, complete: { (peripheral, scrvice, characteristic) in
                
                FzhBluetooth.shareInstance()?.writeValue(self.bytesToStr(bytes: tokenBytes), for: characteristic, completionBlock: { (characteristic, error) in
                    
                    
                }, return: { (peripheral, characteristic, str, error) in
                    
                    
                    
                })
                
            }, fail: { (peripheral, error) in
                
                SVProgressHUD.showError(withStatus: error?.localizedDescription)
                
            })
            
        })
        
        
        
    }
//    func bytesToStr(bytes:[UInt8]) -> String {
//        var hexStr = ""
//        for index in 0 ..< bytes.count {
//            var Str = bytes[index].description
//            if Str.count == 1 {
//                Str = "0 "+Str;
//            }else {
//                let low = Int(Str)!%16
//                let hight = Int(Str)!/16
//                Str = hexIntToStr(HexInt: hight) + hexIntToStr(HexInt: low)
//            }
//            hexStr += Str
//        }
//
////        return hexStr.aes128AndBase64(CCOption, <#T##operation: CCOperation##CCOperation#>, <#T##keyData: Data##Data#>, <#T##iv: Data?##Data?#>)
////        return hexStr.aes128AndBase64(kCCEncrypt, kCCOptionPKCS7Padding, data(from: "3A60432A5C01211F291E0F4E0C132825"), nil)
//    }
    
    func data(from hexStr: String) -> Data {
        let bytes = self.bytes(from: hexStr)
        return Data(bytes: bytes)
    }
    func bytes(from hexStr: String) -> [UInt8] {
        assert(hexStr.count % 2 == 0, "输入字符串格式不对，8位代表一个字符")
        var bytes = [UInt8]()
        var sum = 0
        // 整形的 utf8 编码范围
        let intRange = 48...57
        // 小写 a~f 的 utf8 的编码范围
        let lowercaseRange = 97...102
        // 大写 A~F 的 utf8 的编码范围
        let uppercasedRange = 65...70
        for (index, c) in hexStr.utf8CString.enumerated() {
            var intC = Int(c.byteSwapped)
            if intC == 0 {
                break
            } else if intRange.contains(intC) {
                intC -= 48
            } else if lowercaseRange.contains(intC) {
                intC -= 87
            } else if uppercasedRange.contains(intC) {
                intC -= 55
            } else {
                assertionFailure("输入字符串格式不对，每个字符都需要在0~9，a~f，A~F内")
            }
            sum = sum * 16 + intC
            // 每两个十六进制字母代表8位，即一个字节
            if index % 2 != 0 {
                bytes.append(UInt8(sum))
                sum = 0
            }
        }
        return bytes
    }
    
    func bytesToStr(bytes:[UInt8]) -> String {
        var hexStr = ""
        for index in 0 ..< bytes.count {
            var Str = bytes[index].description
            if Str.count == 1 {
                Str = "0 "+Str;
            }else {
                let low = Int(Str)!%16
                let hight = Int(Str)!/16
                Str = hexIntToStr(HexInt: hight) + hexIntToStr(HexInt: low)
            }
            hexStr += Str
        }
        return hexStr
    }
    
    func hexIntToStr(HexInt:Int) -> String {
        var Str = ""
        if HexInt>9 {
            switch HexInt{
            case 10:
                Str = "A"
                break
            case 11:
                Str = "B"
                break
            case 12:
                Str = "C"
                break
            case 13:
                Str = "D"
                break
            case 14:
                Str = "E"
                break
            case 15:
                Str = "F"
                break
            default:
                Str = "0"
            }
        }else {
            Str = String(HexInt)
        }
        
        return Str
    }

}
