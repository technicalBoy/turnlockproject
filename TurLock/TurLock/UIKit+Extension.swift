//
//  UIKit+Extension.swift
//  ReadyVeg
//
//  Created by WLF on 2019/8/9.
//  Copyright © 2019年 WLF. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import CommonCrypto
let KScreenWidth = UIScreen.main.bounds.size.width

let KScreenHeight = UIScreen.main.bounds.size.height

let W_IphoneX = (CGFloat(KScreenWidth) == CGFloat(375.0) && CGFloat(KScreenHeight) == CGFloat(812.0)) ? true : false
let W_NavBarHeight = W_IphoneX ? CGFloat(88.0) : CGFloat(64.0)
let W_StatusBarHeight = W_IphoneX ? CGFloat(44.0) : CGFloat(20.0)

let Kscale = KScreenWidth/375

let  User_Defaults = UserDefaults.standard

let HDAppDelegate  = UIApplication.shared.delegate as! AppDelegate
let  DevictName = "SmartLock"
extension UIColor {

    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat = 1.0) {
        self.init(displayP3Red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: alpha)
    }

    
}


extension UIViewController{
    
    func getVCFromStoryBoard(name: String) -> UIViewController {
        
        let story = UIStoryboard.init(name: name, bundle: nil)
        
        return story.instantiateViewController(withIdentifier:name)
    }
    
    func checkAlertExist() -> Bool {
        for window: UIWindow in UIApplication.shared.windows {
            if (window.rootViewController?.presentedViewController is UIAlertController) {
                return true
            }
        }
        return false
    }
    
    public func showCustomAlert(title: String?, message: String?, cancelTitle: String = "Cancel", actionTitle: String,  handler: ((UIAlertAction) -> Void)?) {
        if checkAlertExist() {
            
            return
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .default) { alert in }
        alertController.addAction(cancelAction)
        let customAction = UIAlertAction(title: actionTitle, style: .destructive, handler: handler)
        alertController.addAction(customAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    public func showAlert(title: String?, message: String?, actionTitle: String, handler: ((UIAlertAction) -> Void)?) {
        
        if checkAlertExist() {
            
            return
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: actionTitle, style: .default, handler: handler)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    public func showErrorAlert(message: String?) {
        
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    public func showSuccessAlert(message: String?) {
        
        let alertController = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    func showLoading() {
        
        DispatchQueue.main.async {
            let viewHeight = self.view.frame.height
            let viewWidth = self.view.frame.width
            let indicator = UIActivityIndicatorView()
            
            indicator.style = .whiteLarge
            indicator.color = .gray
            indicator.center = CGPoint(x: viewWidth / 2, y: viewHeight / 2)
            indicator.startAnimating()
            
            self.view.addSubview(indicator)
        }
    }
    
    func stopLoading() {
        self.view.subviews.forEach({ (view) in
            if let indicator = view as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        })
    }
}




extension String{
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
}

extension UIView{
    
    /// x
    var x: CGFloat {
        get { return frame.origin.x }
        set(newValue) {
            var tempFrame: CGRect = frame
            tempFrame.origin.x    = newValue
            frame                 = tempFrame
        }
    }
    
    /// y
    var y: CGFloat {
        get { return frame.origin.y }
        set(newValue) {
            var tempFrame: CGRect = frame
            tempFrame.origin.y    = newValue
            frame                 = tempFrame
        }
    }
    
    /// height
    var height: CGFloat {
        get { return frame.size.height }
        set(newValue) {
            var tempFrame: CGRect = frame
            tempFrame.size.height = newValue
            frame                 = tempFrame
        }
    }
    
    /// width
    var width: CGFloat {
        get { return frame.size.width }
        set(newValue) {
            var tempFrame: CGRect = frame
            tempFrame.size.width  = newValue
            frame = tempFrame
        }
    }
    
    /// size
    var size: CGSize {
        get { return frame.size }
        set(newValue) {
            var tempFrame: CGRect = frame
            tempFrame.size        = newValue
            frame                 = tempFrame
        }
    }
    
    /// centerX
    var centerX: CGFloat {
        get { return center.x }
        set(newValue) {
            var tempCenter: CGPoint = center
            tempCenter.x            = newValue
            center                  = tempCenter
        }
    }
    
    /// centerY
    var centerY: CGFloat {
        get { return center.y }
        set(newValue) {
            var tempCenter: CGPoint = center
            tempCenter.y            = newValue
            center                  = tempCenter;
        }
    }
    
    
}

class Util: NSObject {
    class func checkEamilAddress(testStr:String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: testStr)
    }

}
// MARK:-
extension FileManager{
    
    class func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    class func removeItem(fromURL url: URL) {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print(error.localizedDescription)
        }
    }
    
}

enum CryptError: Error {
    case noIV
    case cryptFailed
    case notConvertTypeToData
}

extension Data {
    ///***********加密&解密************///
    //===>>>>>>>AES128
    func dataCryptAES128(_ options: CCOptions?, _ operation: CCOperation, _ keyData: Data, _ iv: Data?)  throws -> Data {
        return try self.dataCrypt(options ?? CCOptions(kCCOptionECBMode),
                                  operation,
                                  keyData,
                                  iv,
                                  CCAlgorithm(kCCAlgorithmAES128))
    }
    //===>>>>>>>基本方法
    func dataCrypt(_ options: CCOptions, _ operation: CCOperation, _ keyData: Data, _ iv: Data?, _ algorithm: UInt32) throws -> Data {
        
        if iv == nil && (options & CCOptions(kCCOptionECBMode)) == 0 {
            print("Error in crypto operation: dismiss iv!")
            throw(CryptError.noIV)
        }
        //key
        let keyBytes = keyData.bytes()
        let keyLength = size_t(kCCKeySizeAES128)
        //data(input)
        let dataBytes = self.bytes()
        let dataLength = size_t(self.count)
        //data(output)
        var buffer = Data(count: dataLength + Int(kCCBlockSizeAES128))
        let bufferBytes = buffer.mutableBytes()
        let bufferLength = size_t(buffer.count)
        //iv
        let ivBuffer: UnsafePointer<UInt8>? = iv == nil ? nil : iv!.bytes()
        
        var bytesDecrypted: size_t = 0
        
        let cryptState = CCCrypt(operation,
                                 algorithm,
                                 options,
                                 keyBytes,
                                 keyLength,
                                 ivBuffer,
                                 dataBytes,
                                 dataLength,
                                 bufferBytes,
                                 bufferLength,
                                 &bytesDecrypted)
        
        guard Int32(cryptState) == Int32(kCCSuccess) else {
            print("Error in crypto operation: \(cryptState)")
            throw(CryptError.cryptFailed)
        }
        
        buffer.count = bytesDecrypted
        return buffer
    }
    
    //===>>>>>>>Help Funcations<<<<<<<===//
    func bytes() -> UnsafePointer<UInt8> {
        return self.withUnsafeBytes { (bytes: UnsafePointer<UInt8>) -> UnsafePointer<UInt8> in
            return bytes
        }
    }
    
    mutating func mutableBytes() -> UnsafeMutablePointer<UInt8> {
        return self.withUnsafeMutableBytes { (bytes: UnsafeMutablePointer<UInt8>) -> UnsafeMutablePointer<UInt8> in
            return bytes
        }
    }
}

extension String{
    //AES128加密之后,base64编码
    func aes128AndBase64(_ options: CCOptions?, _ operation: CCOperation, _ keyData: Data, _ iv: Data?) throws -> String {
        guard let data = self.data(using: .utf8) else {
            throw(CryptError.notConvertTypeToData)
        }
        
        let aesData = try data.dataCryptAES128(options,
                                               operation,
                                               keyData,
                                               iv)
        
        return  String.init(data: aesData, encoding: String.Encoding.utf8)!
    }
}
