//
//  mineSettingVC.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/25.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class mineSettingVC: UIViewController {
    
    @IBOutlet weak var headerBtn: UIButton!
    @IBOutlet weak var Text_NickName: UITextField!
    @IBOutlet weak var Text_Name: UITextField!
    @IBOutlet weak var Text_CardId: UITextField!
    @IBOutlet weak var Text_Address: UITextField!
    @IBOutlet weak var Text_PhoneNum: UITextField!
    @IBOutlet weak var Text_Mail: UITextField!
    
    override func viewDidLoad() {
        
        headerBtn.layer.cornerRadius = headerBtn.height / 2
        headerBtn.layer.masksToBounds = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        /*
         self.userId = json["user_id"].int
         self.mobile = json["mobile"].string
         self.nickName = json["username"].string
         self.truename = json["truename"].string
         self.id_no = json["id_no"].string
         self.address = json["address"].string
         self.email = json["email"].string*/
        let userDict = user_account.currentAccount()
        Text_Mail.text =  (userDict!["email"] as? String)
        Text_Name.text = userDict!["truename"] as? String
        Text_CardId.text = userDict!["id_no"] as? String
        Text_PhoneNum.text =  (userDict!["mobile"] as? String)
        Text_NickName.text = userDict!["username"] as? String
        Text_Address.text = userDict!["address"] as? String
        
    }
    
    
    @IBAction func headerIconClick(_ sender: Any) {
        
        let alertVC = UIAlertController.init(title: "头像", message: "", preferredStyle: .actionSheet)
        
        let camerAction = UIAlertAction.init(title: "相机", style: .default) { (alert) in
            
              self.loadMedium(.camera)
        }
        
        let albumAction = UIAlertAction.init(title: "相册", style: .default) { (alert) in
            
            self.loadMedium(.photoLibrary)
        }
        
        let cancleAction = UIAlertAction.init(title: "取消", style: .cancel) { (alert) in
            
        }
        
        alertVC.addAction(camerAction)
        alertVC.addAction(albumAction)
        alertVC.addAction(cancleAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func loadMedium( _ sourceType: UIImagePickerController.SourceType) {
        
        
        if UIImagePickerController.isSourceTypeAvailable(sourceType){
            
            let picker = UIImagePickerController()
            
            picker.delegate = self
            
            picker.sourceType = sourceType
            
            picker.allowsEditing = true
            
            present(picker, animated:true, completion: { () -> Void in
                
                
                
            })
            
        }else{
            
            debugPrint("error")
            
        }
    }
    
    @IBAction func back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func settingInfo(_ sender: Any) {
        
        if judgeTextFiled() {
            
            Network.share.updateUserInfo(userId: user_account.currentUserID()!, userName: Text_Name.text!, idCard: Text_CardId.text!, pwd: "", address: Text_Address.text!, email: Text_Mail.text!, nickName: Text_NickName.text!, headPortrait: "", tel: Text_PhoneNum.text!, { (json) in
//                "code": 200,
//                "data": "",
//                "msg": "修改成功"
                let dict = json.dictionary
                SVProgressHUD.showSuccess(withStatus: dict!["msg"]?.string)
            }) { (error) in
                
                
            }
            
        }
        
    }
    
}


extension mineSettingVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        picker.dismiss(animated: true, completion: {
                
            self.headerBtn.setBackgroundImage(image, for: .normal)
            self.headerBtn.setTitle("", for: .normal)
        })
    }
    
    
    func judgeTextFiled() -> Bool {
        
        if(Text_NickName.text!.trimmingCharacters(in: .whitespaces) == "" ){
            
            SVProgressHUD.showError(withStatus: "Please enter  nickName..!!")
            
            return false
        }
        else if(Text_Name.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please enter  name..!!")
            
            return false
        } else if(Text_CardId.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please enter  CardId..!!")
            
            return false
        } else if(Text_Address.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please again enter  address..!!")
            
            return false
        }else if(Text_PhoneNum.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please again enter  phone..!!")
            
            return false
        }else if(Text_Mail.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please again enter  mail..!!")
            
            return false
        }
        return true
    }
    
}
