//
//  mineViewController.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/21.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
let section_one_imageArr:[UIImage] = [#imageLiteral(resourceName: "icon_set_data"), #imageLiteral(resourceName: "icon_set_password")]
let section_one_titleArr:[String] = ["资料设置", "密码设置"]

let section_two_imageArr:[UIImage] = [#imageLiteral(resourceName: "icon_app_set"), #imageLiteral(resourceName: "icon_app_set"), #imageLiteral(resourceName: "icon_app_set"), #imageLiteral(resourceName: "icon_app_set")]
let section_two_titleArr:[String] = ["音效设置", "使用说明", "固件设计", "关于我们"]

class mineViewController: UITableViewController {
    
    lazy var headerView: mineHeaderView = {
        
        let headerView = Bundle.main.loadNibNamed("mineHeaderView", owner: nil, options: [:])?.last as! mineHeaderView
        
        return headerView
    }()
    
    lazy var footerView: mineFooterView = {
        
        let footerView = Bundle.main.loadNibNamed("mineFooterView", owner: nil, options: [:])?.last as! mineFooterView
        
        return footerView
    }()
    
    override func viewDidLoad() {
        
        self.tableView.tableHeaderView = headerView
        self.tableView.tableFooterView = footerView
        self.tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        //        requestData()
       
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 2
        }else{
            
            return 4
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if indexPath.section == 0{
            
            cell.imageView?.image = section_one_imageArr[indexPath.row]
            cell.textLabel?.text = section_one_titleArr[indexPath.row]
            
        }else{
            
            cell.imageView?.image = section_two_imageArr[indexPath.row]
            cell.textLabel?.text = section_two_titleArr[indexPath.row]
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 1
        }else{
            
            return 50
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            
            return ""
        }else{
            
            return "应用设置"
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 && indexPath.row == 0{
            
            let strory = UIStoryboard.init(name: "mineSettingVC", bundle: nil)
            let MenuVC = strory.instantiateViewController(withIdentifier: "mineSettingVC") as! mineSettingVC
            
            self.present(MenuVC, animated: true, completion: nil)
            
        }
        
    }
    
}




































































