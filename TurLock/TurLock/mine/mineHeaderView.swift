//
//  mineHeaderView.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/21.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
class mineHeaderView: UIView {
    
    @IBOutlet weak var headImage: UIImageView!
    
    @IBOutlet weak var nickNameLbl: UILabel!
    let BaseUrl = "http://2448932sq8.qicp.vip:59278/"
    override func awakeFromNib() {
        super.awakeFromNib()
        setData()
    }
    
    func setData() -> Void {
        
        let username =  User_Defaults.dictionary(forKey: "user_account")
        nickNameLbl.text = username!["mobile"] as? String
        guard let avatar = username!["avatar"] as? String else { return  }
        
        
        let stringAva = BaseUrl + avatar
        
        let url = URL(string:stringAva)
        headImage.kf.setImage(with: url,placeholder: UIImage.init(named: "icon_accont"))
        
    }
    
}
