//
//  registAccountVC.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/23.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class registAccountVC: UIViewController {
    
    @IBOutlet weak var phoneNumText: UITextField!
     @IBOutlet weak var verbPassWordText: UITextField!
     @IBOutlet weak var passWordText: UITextField!
     @IBOutlet weak var passWordAgainText: UITextField!
     @IBOutlet weak var textView: UIView!
    var verbCode: String = ""
    lazy var sendVerdBtn: UIButton = {
        
        let sendVerdBtn = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 120 * Kscale, height: 40))
        
        sendVerdBtn.setTitle("发送验证码", for: .normal)
        
        sendVerdBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        sendVerdBtn.layer.borderWidth = 0.5
        sendVerdBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        sendVerdBtn.setTitleColor(UIColor.black, for: .normal)
        sendVerdBtn.addTarget(self, action: #selector(verifyPassWord), for: .touchUpInside)
        return sendVerdBtn
    }()
    override func viewDidLoad() {
        
        self.verbPassWordText.rightView = sendVerdBtn
        self.verbPassWordText.rightViewMode = .always
        self.textView.layer.cornerRadius = 8
        self.textView.layer.masksToBounds = true
    }
    
    @objc func verifyPassWord(){
        
        if phoneNumText.text == "" {
            SVProgressHUD.showError(withStatus: "手机号不能为空")
            return
        }
        
        Network.share.sendsms(tel: phoneNumText.text!, { (json) in
            
            self.verbCode = (json.dictionary?["data"]?["code"].string ?? "")
            self.timeChange()
            SVProgressHUD.showSuccess(withStatus: json.dictionary?["msg"]?.string)
            SVProgressHUD.dismiss(withDelay: 1)
        }) { (error) in
            
            
        }
        
    }
    
    @IBAction func back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    func timeChange() {
        
        var time = 60
        let codeTimer = DispatchSource.makeTimerSource(flags: .init(rawValue: 0), queue: DispatchQueue.global())
        codeTimer.schedule(deadline: .now(), repeating: .milliseconds(1000))  //此处方法与Swift 3.0 不同
        codeTimer.setEventHandler {
            
            time = time - 1
            
            DispatchQueue.main.async {
                self.sendVerdBtn.isEnabled = false
            }
            
            if time < 0 {
                codeTimer.cancel()
                DispatchQueue.main.async {
                    self.sendVerdBtn.isEnabled = true
                    self.sendVerdBtn.setTitle("重新发送", for: .normal)
                }
                return
            }
            
            DispatchQueue.main.async {
                self.sendVerdBtn.setTitle("\(time) 秒", for: .normal)
            }
            
        }
        
        codeTimer.activate()
        
    }
    
    @IBAction func registAccountBtnClick(_ sender: Any) {
        
        if judgeTextFiled() {
            
            if verbCode != verbPassWordText.text && verbCode != "" {
                
                  return SVProgressHUD.showError(withStatus: "验证码错误")
            }
            
            if passWordText.text != passWordAgainText.text {
                
                return SVProgressHUD.showError(withStatus: "两次密码输入不正确")
                
            }
           
            Network.share.regist(tel: phoneNumText.text!, pwd: passWordText.text!, code:verbPassWordText.text! ,{ (json) in
            
                print(json)
                self.dismiss(animated: true, completion: nil)
                
            }) { (error_json) in
                
                SVProgressHUD.showError(withStatus: error_json)
            }
            
            
        }
        
    }
    
    func judgeTextFiled() -> Bool {
        
        if(phoneNumText.text!.trimmingCharacters(in: .whitespaces) == "" ){
            
            SVProgressHUD.showError(withStatus: "Please enter  phoneNum..!!")
            
            return false
        }
        else if(verbPassWordText.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please enter  verify..!!")
            
            return false
        } else if(passWordText.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please enter  passWord..!!")
            
            return false
        } else if(passWordAgainText.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please again enter  passWord..!!")
            
            return false
        }
        
        return true
    }
}
