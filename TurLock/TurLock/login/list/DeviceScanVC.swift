//
//  DeviceScanVC.swift
//  TurLock
//
//  Created by wlf on 2019/9/8.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import swiftScan
class DeviceScanVC: LBXScanViewController {
    
    /**
     @brief  扫码区域上方提示文字
     */
    var topTitle: UILabel?
    
    //底部显示的功能项
    var bottomItemsView: UIView?
    
    //编号添加
    var btnPhoto: UIButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //需要识别后的图像
        setNeedCodeImage(needCodeImg: true)
        
        //框向上移动10个像素
        scanStyle?.centerUpOffset += 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        topTitle?.text = "添加设备"
        self.setNav()
        
    }
    
    func setNav() {
    
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 100, height: 30))
        
        label.text = "添加设备"
        label.font = UIFont.systemFont(ofSize: 21)
        label.textAlignment = .center
        label.textColor = UIColor.white
        self.navigationItem.titleView = label
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        drawBottomItems()
    }
    
    func drawBottomItems() {
        if (bottomItemsView != nil) {
            
            return
        }
        
        let yMax = self.view.frame.maxY - self.view.frame.minY
        
        bottomItemsView = UIView(frame: CGRect(x: 0.0, y: yMax-100, width: self.view.frame.size.width, height: 100 ) )
        
        bottomItemsView!.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        
        self.view .addSubview(bottomItemsView!)
        
        let size = CGSize(width: 40, height: 40)
        
        self.btnPhoto = UIButton()
        btnPhoto.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        btnPhoto.center = CGPoint(x: bottomItemsView!.frame.width/2, y: bottomItemsView!.frame.height/2)
        
        btnPhoto.setImage(UIImage(named: "CodeScan.bundle/add_device"), for:UIControl.State.normal)
        btnPhoto.addTarget(self, action: #selector(self.addDeviceFromModel), for: UIControl.Event.touchUpInside)
        
        bottomItemsView?.addSubview(btnPhoto)
        self.view .addSubview(bottomItemsView!)
        
    }
    
    //编码添加
    @objc func addDeviceFromModel() {
        
        
        
    }
    
   
    
}
