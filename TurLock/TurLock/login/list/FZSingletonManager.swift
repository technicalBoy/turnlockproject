//
//  FZSingletonManager.swift
//  TurLock
//
//  Created by wlf on 2019/9/22.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import CoreBluetooth
class FZSingletonManager: NSObject {
    
    var GPrint_Peripheral: CBPeripheral? //当前链接
    var GPrint_Chatacter: CBCharacteristic?//当前Chatacter
    var Gprint_readChatacter: CBCharacteristic? 
    static var share = FZSingletonManager()
}
