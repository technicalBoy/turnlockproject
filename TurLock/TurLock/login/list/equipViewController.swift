//
//  equipViewController.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/21.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import SVProgressHUD
import swiftScan
import FZBluetooth

class equipViewController: UITableViewController, LBXScanViewControllerDelegate{
    
    var device_model: [DeviceModel]?
    
    lazy var style: LBXScanViewStyle = {
        
        var style = LBXScanViewStyle()
        style.centerUpOffset = 60;
        style.xScanRetangleOffset = 30;
        if UIScreen.main.bounds.size.height <= 480 {
            //3.5inch 显示的扫码缩小
            style.centerUpOffset = 40;
            style.xScanRetangleOffset = 20;
        }
        style.color_NotRecoginitonArea = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 0.4)
        style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle.Inner;
        style.photoframeLineW = 2.0;
        style.photoframeAngleW = 16;
        style.photoframeAngleH = 16;
        style.isNeedShowRetangle = false;
        style.anmiationStyle = LBXScanViewAnimationStyle.NetGrid
        style.animationImage = UIImage(named: "scan.bundle/scan_net")
        return style
    }()
    lazy var tableBgView: UIImageView = {
        
        let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: KScreenWidth, height: KScreenHeight))
        
        imageView.image  = #imageLiteral(resourceName: "icon_bg")
        return imageView
    }()
    
    override func viewDidLoad() {
        
        self.tableView.register(UINib.init(nibName: "equipCell", bundle: nil), forCellReuseIdentifier: "equipCell")
        self.tableView.backgroundView = tableBgView
        self.tableView.separatorStyle = .none
        self.navigationController?.navigationBar.setBackgroundImage(createImageWithColor(color: UIColor.clear), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = createImageWithColor(color: UIColor.clear)
        self.requestData()
    }
    func createImageWithColor(color:UIColor)->UIImage
    {
        let rect=CGRect.init(x: 0, y: 0, width: 1, height: 1);
        
        UIGraphicsBeginImageContext(rect.size);
        let context = UIGraphicsGetCurrentContext();
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let theImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        return theImage!;
        
    }
    func setScanManager() {
        //设置扫码区域参数
        let vc = DeviceScanVC()
        var style = LBXScanViewStyle()
        style.animationImage = UIImage(named: "CodeScan.bundle/qrcode_scan_light_green")
        vc.scanStyle = style
        self.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: false)
        self.hidesBottomBarWhenPushed = false
    }
  
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        
        let scanCode =  scanResult.strScanned
        
        Network.share.addEquipment(userId: user_account.currentUserID()!, deviceName: "TurLock", deviceModel: scanCode!, { (json) in
            
            self.device_model?.append(DeviceModel.init(json: json)!)
        }) { (error) in
            
            SVProgressHUD.showError(withStatus: error)
        }
    }
    func requestData() {
        
        SVProgressHUD.show()
        self.device_model = []
        Network.share.queryAllEquipmentList(userId: user_account.currentUserID()!, { (json) in
            SVProgressHUD.dismiss()
            
            let jsonData = json["data"]["devices"].array
            
            for json: JSON in jsonData!{
                self.device_model?.append(DeviceModel.init(json: json)!)
            }
            
            self.tableView.reloadData()
        }) { (error) in
            
            SVProgressHUD.dismiss()
            SVProgressHUD.showError(withStatus: error)
        }
    
    }
    
    @IBAction func addEquipClick(_ sender: Any) {
        
        
        setScanManager()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.device_model?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "equipCell", for: indexPath) as!  equipCell
        let model = self.device_model?[indexPath.row]
        cell.backgroundColor = UIColor.init(white: 1, alpha: 0)
        cell.selectionStyle = .none
        cell.nameLabel.text = model?.deviceName
        cell.modelLabel.text = model?.deviceModel
        cell.timeLabel.text  = model?.add_time
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 130
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let strory = UIStoryboard.init(name: "equipDetailVC", bundle: nil)
        let MenuVC = strory.instantiateViewController(withIdentifier: "equipDetailVC")
        self.navigationController?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(MenuVC, animated: true)
        self.navigationController?.hidesBottomBarWhenPushed = false
    }
    
}
