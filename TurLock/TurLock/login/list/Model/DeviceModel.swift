//
//  deviceModel.swift
//  TurLock
//
//  Created by wlf on 2019/8/29.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import SwiftyJSON
class DeviceModel: NSObject {
    
    /*deviceId": 2,
     "userId": 1,
     "deviceName": "设备2",
     "deviceModel": "",
     "createTime": "2019-07-27 12:12:12",
     "deviceNum": "ST10015026"
     
     */
//    var userId: Int?
//    var deviceId: Int?
//    var deviceName: String?
//    var createTime: String?
//    var deviceModel: String?
//    var deviceNum: String?
        var user_id: Int?
        var deviceName: String?
        var add_time: String?
        var deviceModel: String?
    
    init?(json: JSON) {
        
        self.user_id = json["userId"].int
        
        self.deviceName = json["name"].string
        
        self.deviceModel = json["model"].string
    self.add_time = json["add_time"].string
    }
    
    
}
