//
//  equipDetailVC.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/22.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import FZBluetooth
import SVProgressHUD

class equipDetailVC: UIViewController {
    
    var centralManager: CBCentralManager?
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var equipInfoView: UIView!
    var blueListArr: [bluethModel] = []
    var deviceModel: DeviceModel?{
        
        didSet{
            
            timeLabel.text = deviceModel?.add_time
        }
        
    }
    override func viewDidLoad() {
        
        equipInfoView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(tap)))
        
        FzhBluetooth.shareInstance()?.createAutomaticConnectionEquipmen(withSetOrDelate: SetAutomaticConnectionEquipmen, peripheral: nil)
        FzhBluetooth.shareInstance()?.delegate = self
        
        FzhBluetooth.shareInstance()?.returnState({ (state) in
            
            if state != 5 {
                
                SVProgressHUD.showError(withStatus: "没有开启蓝牙")
                
            }else{
                
                self.scanBluetooths()
                let uuidStr = UserDefaults.standard.string(forKey: "DeviceUUID") ?? ""
                
                if uuidStr.count > 0{
                    
                    let peripheral = FzhBluetooth.shareInstance()?.retrievePeripheral(withUUIDString: uuidStr)
                    
                    FzhBluetooth.shareInstance()?.uuidString = "FFF0"
                    self.autoCollectBluetoothWithPeripheral(p: peripheral!)
                }
                
            }
            
        })
//        NotificationCenter.default.addObserver(self, selector: #selector(self.didHaveAutoConnection()), name: NSNotification.Name.init(PostAutoConnectionNotificaiton), object: nil)
        
    }
    
    func scanBluetooths()  {
        FzhBluetooth.shareInstance()?.scanForPeripherals(withPrefixName: DevictName, discoverPeripheral: { (centralManager, peripheral, advertisementData, RSSI) in
            
            print("搜索到设备了")
            var perpheralIndex = -1
            
            if self.blueListArr.count == 0 {
                let model = bluethModel.init()
                model.blueName = peripheral?.name
                model.peripheral = peripheral
                model.UUIDString = peripheral?.identifier.uuidString
                print(model.UUIDString)
                //if model.UUIDString == "FFF0"{
                
                    self.autoCollectBluetoothWithPeripheral(p: peripheral!)
                //}
                self.blueListArr.append(model)
            }else{
                
                for i in 0...(self.blueListArr.count) - 1 {
                    
                    let model = self.blueListArr[i]
                    if model.peripheral?.identifier == peripheral?.identifier{
                        perpheralIndex = i
                        break
                        
                    }
                    
                }
                
            }
            
        })
    }
    
    @IBAction func lockBtnClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        //开锁
        
        let unlockDict = FzhBluetooth.shareInstance()?.writeValue("", for: FZSingletonManager.share.GPrint_Chatacter)
        
        print("unlock: \(unlockDict)")
        //关锁
        let lockDict = FzhBluetooth.shareInstance()?.writeValue("", for: FZSingletonManager.share.GPrint_Chatacter)
        print("lockDict: \(lockDict)")
    }
    func autoCollectBluetoothWithPeripheral(p: CBPeripheral) {
        
        FzhBluetooth.shareInstance()?.stopScan()
        FzhBluetooth.shareInstance()?.connect(p, complete: { (peripheral, server, characteristic) in
            
            print("连接成功")
            
            
            
            let model = bluethModel.init()
            model.blueName = peripheral?.name
            model.peripheral = peripheral
            model.UUIDString = peripheral?.identifier.uuidString
            
            FZSingletonManager.share.GPrint_Chatacter = FzhBluetooth.shareInstance()?.writeCharacteristic
            FZSingletonManager.share.GPrint_Peripheral = peripheral
            
            User_Defaults.set(model.UUIDString, forKey: "DeviceUUID")
            
            //获取token
            let unlockDict = FzhBluetooth.shareInstance()?.writeValue(FzhString.sharedInstance()?.fzHexString(from: Data.init(bytes: tokenBytes)), for: FZSingletonManager.share.GPrint_Chatacter) //FzhBluetooth.shareInstance()?.writeValue(FzhString.sharedInstance()?.fzHexString(from: Data.init(bytes: tokenBytes))), for: FZSingletonManager.share.GPrint_Chatacter;)
            //获取电量
            let lockDict = FzhBluetooth.shareInstance()?.writeValue(self.bytesToStr(bytes: unlockBytes), for: FZSingletonManager.share.GPrint_Chatacter)
            
            //查询锁状态
            
            //查询电量
            
            
            
        }, fail: { (p, error) in
            
            print("连接失败")
        })
        
    }
    
   
    
    func didHaveAutoConnection() {
        
        
    }
    
    @objc func tap(){
        
        let vc = equipeRecordVC()
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
//        TurLockBluetoothManager.share.bluetoohStar()
        
        //设置电量
        
        
        //扫描外设
//        FzhBluetooth.shareInstance()?.scanForPeripherals(withPrefixName: DevictName, discoverPeripheral: { (centralManager, peripheral, hashable, RSSINum) in
//
//            self.centralManager = centralManager
//
//            //链接外设
//            FzhBluetooth.shareInstance()?.connect(peripheral, complete: { (peripheral, scrvice, characteristic) in
//
//                if characteristic?.uuid.uuidString == "FFF1" {
//
//
//                }else if characteristic?.uuid.uuidString == "FFF2"{
//
//
//                }
//
//                FzhBluetooth.shareInstance()?.writeValue(self.bytesToStr(bytes: tokenBytes), for: characteristic, completionBlock: { (characteristic, error) in
//
//
//
//                }, return: { (peripheral, characteristic, str, error) in
//
//
//
//                })
//
//            }, fail: { (peripheral, error) in
//
//                SVProgressHUD.showError(withStatus: error?.localizedDescription)
//
//            })
//
//        })
        
        
    }
    func data(from hexStr: String) -> Data {
        let bytes = self.bytes(from: hexStr)
        return Data(bytes: bytes)
    }
    func bytes(from hexStr: String) -> [UInt8] {
        assert(hexStr.count % 2 == 0, "输入字符串格式不对，8位代表一个字符")
        var bytes = [UInt8]()
        var sum = 0
        // 整形的 utf8 编码范围
        let intRange = 48...57
        // 小写 a~f 的 utf8 的编码范围
        let lowercaseRange = 97...102
        // 大写 A~F 的 utf8 的编码范围
        let uppercasedRange = 65...70
        for (index, c) in hexStr.utf8CString.enumerated() {
            var intC = Int(c.byteSwapped)
            if intC == 0 {
                break
            } else if intRange.contains(intC) {
                intC -= 48
            } else if lowercaseRange.contains(intC) {
                intC -= 87
            } else if uppercasedRange.contains(intC) {
                intC -= 55
            } else {
                assertionFailure("输入字符串格式不对，每个字符都需要在0~9，a~f，A~F内")
            }
            sum = sum * 16 + intC
            // 每两个十六进制字母代表8位，即一个字节
            if index % 2 != 0 {
                bytes.append(UInt8(sum))
                sum = 0
            }
        }
        return bytes
    }
    
    func bytesToStr(bytes:[UInt8]) -> String {
        var hexStr = ""
        for index in 0 ..< bytes.count {
            var Str = bytes[index].description
            if Str.count == 1 {
                Str = "0 "+Str;
            }else {
                let low = Int(Str)!%16
                let hight = Int(Str)!/16
                Str = hexIntToStr(HexInt: hight) + hexIntToStr(HexInt: low)
            }
            hexStr += Str
        }
        return hexStr
    }
    
    func hexIntToStr(HexInt:Int) -> String {
        var Str = ""
        if HexInt>9 {
            switch HexInt{
            case 10:
                Str = "A"
                break
            case 11:
                Str = "B"
                break
            case 12:
                Str = "C"
                break
            case 13:
                Str = "D"
                break
            case 14:
                Str = "E"
                break
            case 15:
                Str = "F"
                break
            default:
                Str = "0"
            }
        }else {
            Str = String(HexInt)
        }
        
        return Str
    }

    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
         self.tabBarController?.tabBar.isHidden = false
        
    }
    
    @IBAction func backBtnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
}
