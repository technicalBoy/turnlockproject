//
//  equipCell.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/21.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
class equipCell: UITableViewCell {
    
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var modelLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        coverView.layer.cornerRadius = 8
        coverView.layer.masksToBounds = true
    }
    
}
