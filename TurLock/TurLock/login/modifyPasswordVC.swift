//
//  modifyPasswordVC.swift
//  TurLock
//
//  Created by WLF on 2019/8/23.
//  Copyright © 2019年 WLF All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class modifyPasswordVC: UIViewController {
    
    @IBOutlet weak var modify_phoneText: UITextField!
    @IBOutlet weak var modify_verbText: UITextField!
    @IBOutlet weak var modify_passWordText: UITextField!
    @IBOutlet weak var modify_passWordAgainText: UITextField!
     @IBOutlet weak var modify_textView: UIView!
    var verbCode: String = ""
    lazy var sendVerdBtn: UIButton = {
        
        let sendVerdBtn = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 120 * Kscale, height: 40))
        
        sendVerdBtn.setTitle("发送验证码", for: .normal)
        
        sendVerdBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        sendVerdBtn.layer.borderWidth = 0.5
        sendVerdBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        sendVerdBtn.setTitleColor(UIColor.black, for: .normal)
        sendVerdBtn.addTarget(self, action: #selector(verifyPassWord), for: .touchUpInside)
        return sendVerdBtn
    }()
    override func viewDidLoad() {
        
        self.modify_verbText.rightView = sendVerdBtn
        self.modify_verbText.rightViewMode = .always
        self.modify_textView.layer.cornerRadius = 8
        self.modify_textView.layer.masksToBounds = true
    }
    
    @objc func verifyPassWord(){
        
        if modify_phoneText.text == "" {
            SVProgressHUD.showError(withStatus: "手机号不能为空")
            return
        }
        Network.share.sendsms(tel: modify_phoneText.text!, { (json) in
            
            self.verbCode = (json.dictionary?["data"]?["code"].string ?? "")
            self.timeChange()
            SVProgressHUD.showSuccess(withStatus: json.dictionary?["msg"]?.string)
            SVProgressHUD.dismiss(withDelay: 1)
        }) { (error) in
               SVProgressHUD.showError(withStatus: error)
            
        }
        
    }
    
    @IBAction func backBtnClick(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    func timeChange() {
        
        var time = 60
        let codeTimer = DispatchSource.makeTimerSource(flags: .init(rawValue: 0), queue: DispatchQueue.global())
        codeTimer.schedule(deadline: .now(), repeating: .milliseconds(1000))  //此处方法与Swift 3.0 不同
        codeTimer.setEventHandler {
            
            time = time - 1
            
            DispatchQueue.main.async {
                self.sendVerdBtn.isEnabled = false
            }
            
            if time < 0 {
                codeTimer.cancel()
                DispatchQueue.main.async {
                    self.sendVerdBtn.isEnabled = true
                    self.sendVerdBtn.setTitle("重新发送", for: .normal)
                }
                return
            }
            
            DispatchQueue.main.async {
                self.sendVerdBtn.setTitle("\(time) 秒", for: .normal)
            }
            
        }
        
        codeTimer.activate()
        
    }
    
    @IBAction func modifyBtnClick(_ sender: Any) {
        if verbCode != modify_verbText.text && verbCode != "" {
            
            return SVProgressHUD.showError(withStatus: "验证码错误")
        }
        
        if modify_passWordText.text != modify_passWordAgainText.text {
            
            return SVProgressHUD.showError(withStatus: "两次密码输入不正确")
            
        }
        if  judgeTextFiled() {
            
            Network.share.editpass(tel: modify_phoneText.text!, pwd: modify_passWordText.text!,code:modify_verbText.text!, { (json) in
                
                SVProgressHUD.showSuccess(withStatus: json["msg"].string)
                self.dismiss(animated: true, completion: nil)
                
            }) { (error) in
                
                SVProgressHUD.showError(withStatus: error)
                
            }
            
        }
        
        
    }
    
    func judgeTextFiled() -> Bool {
        
        if(modify_phoneText.text!.trimmingCharacters(in: .whitespaces) == "" ){
            
            SVProgressHUD.showError(withStatus: "Please enter  phoneNum..!!")
            
            return false
        }
        else if(modify_verbText.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please enter  verify..!!")
            
            return false
        } else if(modify_passWordText.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please enter  passWord..!!")
            
            return false
        } else if(modify_passWordAgainText.text!.trimmingCharacters(in: .whitespaces) == ""){
            
            SVProgressHUD.showError(withStatus: "Please again enter  passWord..!!")
            
            return false
        }
        
        return true
    }
}
