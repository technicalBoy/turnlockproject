//
//  account.swift
//  TurLock
//
//  Created by wlf on 2019/8/26.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import SwiftyJSON
class user_account: NSObject {

    var userId: Int?
    var truename: String?
    var pwd: String?
    var email: String?
    var address: String?
    var nickName: String?
    var mobile: String?
    var id_no: String?
    var avatar:String?
    
    
    init?(json: JSON) {

        self.userId = json["user_id"].int
        self.mobile = json["mobile"].string
        self.nickName = json["username"].string
        self.truename = json["truename"].string
        self.id_no = json["id_no"].string
        self.address = json["address"].string
        self.email = json["email"].string
        self.avatar = json["avatar"].string
//        self.pwd = json["pwd"].string
    }
    
    func save() {
        
        var accountDict: [String: Any] = [:]
        
        if self.userId != nil {
            accountDict["user_id"] = userId
        }
        
        if self.nickName != nil {
            accountDict["username"] = nickName
        }
        
//        if self.pwd != nil {
//            accountDict["pwd"] = pwd
//        }
        
        if self.email != nil {
            accountDict["email"] = email
        }
        
        if self.address != nil {
            accountDict["address"] = address
        }
        
        if self.truename != nil {
            accountDict["truename"] = truename
        }
        
        if self.mobile != nil {
            accountDict["mobile"] = mobile
        }
        if self.id_no != nil {
            accountDict["id_no"] = id_no
        }
        
        if self.avatar != nil{
            accountDict["avatar"] = avatar
        }
        
        User_Defaults.set(accountDict, forKey: "user_account")
    }

    class func currentAccount() -> [String : Any]? {
       return  User_Defaults.dictionary(forKey: "user_account")
        
    }
    
    class func currentUserID() -> Int? {
        if  let dict =  User_Defaults.dictionary(forKey: "user_account") {
            
            return dict["user_id"] as? Int
            
        }else{
            
            return 0
        }
    }
    
}
