//
//  loginViewController.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/21.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class loginViewController: UIViewController {
    
    @IBOutlet weak var phoneText: UITextField!
    
    @IBOutlet weak var pwdText: UITextField!
    
    @IBOutlet weak var textBgView: UIView!
    
    var accountModel: user_account?
    override func viewDidLoad() {
        
        self.textBgView.layer.cornerRadius = 8
        self.textBgView.layer.masksToBounds = true
        
    }
    
    
    @IBAction func loginBtnClick(_ sender: Any) {
        
        if judgeTextFiled() {
            SVProgressHUD.show()
            Network.share.login(userName: phoneText.text!, pwd: pwdText.text!, { (json) in
                SVProgressHUD.dismiss()
                self.showSuccessAlert(message: "登陆成功")
                
                self.accountModel = user_account.init(json: json["data"]["userinfo"])
                
                self.accountModel?.save()
                
                let mainTabbar = mainTabbarController()
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window!.rootViewController = mainTabbar
            }) { (json) in
                
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: json)
                
            }
        }
        
     
        
    }
    
    @IBAction func registBtnClick(_ sender: Any) {
        
        let strory = UIStoryboard.init(name: "registAccountVC", bundle: nil)
        let MenuVC = strory.instantiateViewController(withIdentifier: "registAccountVC")
        self.present(MenuVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func forgetPassWordClick(_ sender: Any) {
        
        let strory = UIStoryboard.init(name: "modifyPasswordVC", bundle: nil)
        let MenuVC = strory.instantiateViewController(withIdentifier: "modifyPasswordVC")
        self.present(MenuVC, animated: true, completion: nil)
    }
    
    
    func judgeTextFiled() -> Bool {
        
        if(phoneText.text!.trimmingCharacters(in: .whitespaces) == "" ){
            
            SVProgressHUD.showError(withStatus: "Please enter  phoneNum..!!")
            
            return false
        }
        else if(pwdText.text!.trimmingCharacters(in: .whitespaces) == ""){
            
             SVProgressHUD.showError(withStatus: "Please enter  passWord..!!")
            
            return false
        }
        
        return true
    }
    
}
