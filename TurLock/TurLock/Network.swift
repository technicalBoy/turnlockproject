//
//  Network.swift
//  TurLock
//
//  Created by 王龙飞 on 2019/8/25.
//  Copyright © 2019年 王龙飞. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
typealias json_success = (_ SuccessDict:JSON)->()
typealias json_fail   = (_ FailDict:String)->()

struct api_error {
    static let networkError: String = "No connection available. Please, verify your conectivity and try again."
    static let serviceError: String = "Service unavailable. Please try again later."
}
class Network: NSObject {
      static let share: Network = Network()
    
    let BaseUrl = "http://2448932sq8.qicp.vip:59278/api/"
    
    //tel  pwd  address  userRole
    
    /// 用户注册
    func regist(tel: String, pwd: String, address: String = "广东省深圳市南山区粤海街道麻雀岭中钢大厦M-8",code: String, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["mobile": tel, "password": pwd,  "code": code] as [String : Any]
        Alamofire.request("http://2448932sq8.qicp.vip:59278/api/user/register", method: .post, parameters: mutableDict).responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                print(value)
                let jsonData = JSON(value).dictionary
                if let status = jsonData!["code"] {
                    if status == 200{
                        success(JSON(value))
//                        SVProgressHUD.showSuccess(withStatus: jsonData!["msg"]?.string)
                    }else{
                        
                        fail((jsonData!["msg"]?.string)!)
                    }
                }else{
                    
                    fail(api_error.serviceError)
                }
                
            case .failure(_):
                //                    fail(JSON(error))var error = ""
                var errorStr = ""
                if NetworkReachabilityManager()?.isReachable == false {
                    errorStr = api_error.networkError
                }else{
                    errorStr = api_error.serviceError
                }
                fail(errorStr)
            }
        }
    }
    
    /// 修改密码
    func editpass(tel: String, pwd: String, address: String = "广东省深圳市南山区粤海街道麻雀岭中钢大厦M-8",code: String, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["mobile": tel, "password": pwd,  "code":code] as [String : Any]
                requestData(url: BaseUrl + "/user/editpass", parame: mutableDict, success, fail)


    }
    /// 发送a验证码
    func sendsms(tel: String, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["mobile": tel] as [String : Any]
        requestData(url: BaseUrl + "/user/sendsms", parame: mutableDict, success, fail)
        
    }
    
    ///  用户登录
    func login(userName: String, pwd: String, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict: Parameters = ["mobile": userName, "password": pwd] as [String : Any]
//        requestData(url: BaseUrl + "/user/login", parame: mutableDict, success, fail)
        Alamofire.request(BaseUrl + "user/login", method: .post, parameters: mutableDict).responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                print(value)
                let jsonData = JSON(value).dictionary
                if let status = jsonData!["code"] {
                    if status == 200{
                        success(JSON(value))
//                        SVProgressHUD.showSuccess(withStatus: jsonData!["msg"]?.string)
                    }else{
                        
                        fail((jsonData!["msg"]?.string)!)
                    }
                }else{
                    
                    fail(api_error.serviceError)
                }
                
            case .failure(_):
                //                    fail(JSON(error))var error = ""
                var errorStr = ""
                if NetworkReachabilityManager()?.isReachable == false {
                    errorStr = api_error.networkError
                }else{
                    errorStr = api_error.serviceError
                }
                fail(errorStr)
            }
        }
    }
    
        /// 查询单个用户信息
    func queryUserByUserId(userId: Int, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["user_id": userId] as [String : Int]
        requestData(url: BaseUrl + "queryUserByUserId", parame: mutableDict, success, fail)
    }
      /// 修改用户信息
    func updateUserInfo(userId: Int, userName: String,  idCard: String, pwd:String,  address: String, email: String, nickName: String, headPortrait: String, tel: String, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["user_id": userId, "nick": nickName, "truename": userName, "id_no": idCard, "address": address, "email": email] as [String : Any]

        requestData(url: BaseUrl + "/user/set_info", parame: mutableDict, success, fail)
    }
       /// 上传头像
    func uploadAvatars(userId: Int, avatar: String, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["user_id": userId, "avatar": avatar] as [String : Any]
        
        requestData(url: BaseUrl + "/user/set_avatar", parame: mutableDict, success, fail)
    }
    
       ///  添加设备
    func addEquipment(userId: Int, deviceName: String,  deviceModel: String, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["user_id": userId, "name": deviceName, "model": deviceModel] as [String : Any]
        
        requestData(url: BaseUrl + "/dev/add", parame: mutableDict, success, fail)
    }
   ///  修改设备
    func updateEquipment(userId: Int, deviceName: String,  deviceModel: String, useTime:String,  deviceNum: String, deviceId: Int, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["user_id": userId, "deviceName": deviceName, "deviceModel": deviceModel, "useTime": useTime, "deviceNum": deviceNum, "deviceId": deviceId] as [String : Any]
        
        requestData(url: BaseUrl + "updateEquipment", parame: mutableDict, success, fail)
    }
   ///  查询设备列表
    func queryAllEquipmentList(userId: Int, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        let mutableDict = ["user_id": userId] as [String : Any]
        
        requestData(url: BaseUrl + "/dev/show", parame: mutableDict, success, fail)
    }
    func requestData(url: String, parame: Parameters, _ success: @escaping json_success, _ fail: @escaping json_fail) {
        
        Alamofire.request(url, method: .post, parameters: parame, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                print(value)
                let jsonData = JSON(value).dictionary
                if let status = jsonData!["code"] {
                    if status == 200{
                        success(JSON(value))
//                        SVProgressHUD.showSuccess(withStatus: jsonData!["msg"]?.string)
                    }else{
                        
                        fail((jsonData!["msg"]?.string)!)
                    }
                }else{
                    
                    fail(api_error.serviceError)
                }
                
            case .failure(_):
                //                    fail(JSON(error))var error = ""
                var errorStr = ""
                if NetworkReachabilityManager()?.isReachable == false {
                    errorStr = api_error.networkError
                }else{
                    errorStr = api_error.serviceError
                }
                fail(errorStr)
            }
        }
        
    }
}

/**/
